from tokens.token_type import TokenType
from tokens.keyword import Keyword
from syntax_analyzer.grammar import Grammar

#
# JackTokenizer: Ignores all comments and white space in the input stream,
# and serializes it into Jack-language tokens. The token types are specified
# according to the Jack grammar.
#
class Tokenizer:
    #
    # Opens the input .jack file and gets ready to tokenize it.
    #
    def __init__(self, filename):
        self.input_file = filename
        self.tok_type = None
        self.token = None

        program_lines = self.__read_program_file(filename)
        self.program_text = self.__filter_program_lines(program_lines)

    #
    # are there more tokens in the input.
    #
    def has_more_tokens(self):
        return not self.__is_blank(self.program_text)

    #
    # Gets the next token from the input, and makes it the current token.
    # This method should be called only if has_more_tokens is true. Initially
    # there is no current token.
    #
    def advance(self):
        if self.has_more_tokens():
            self.token = self.__read_token(self.program_text)
            # Debug info
            # print(self.tok_type)
            # print(self.token)
        else:
            self.token = None
            self.tok_type = None

    #
    # Returns the type of the current token, as a constant.
    #
    def token_type(self):
        return self.tok_type

    #
    # Returns the keyword which is the current token, as a constant.
    # This method should be called only if tokenType is KEYWORD.
    #
    def keyword(self):
        if self.token_type() is TokenType.KEYWORD:
            return Keyword(self.token)
        return None

    #
    # Returns the character which is the current token. Should be called only
    # if token_type is SYMBOL.
    #
    def symbol(self):
        if self.token_type() is TokenType.SYMBOL:
            return str(self.token)
        return None

    #
    # Returns the character which is the current token. Should be called only
    # if token_type is IDENTIFIER.
    #
    def identifier(self):
        if self.token_type() is TokenType.IDENTIFIER:
            return str(self.token)
        return None

    #
    # Returns the character which is the current token. Should be called only
    # if token_type is INT_CONST.
    #
    def int_val(self):
        if self.token_type() is TokenType.INT_CONST:
            return int(self.token)
        return None

    #
    # Returns the character which is the current token. Should be called only
    # if token_type is STRING_CONST.
    #
    def string_val(self):
        if self.token_type() is TokenType.STRING_CONST:
            return str(self.token)
        return None



    ########################################################################
    #                       Private methods                                #
    ########################################################################

    #
    # TODO: resolve issue, when we pass space into the method
    #
    def __read_token(self, program_text):
        current_character = 0
        character = program_text[current_character]

        if Grammar.is_symbol(character):
            # Debug info
            #print('symbol ' + character)
            self.tok_type = TokenType.SYMBOL
            self.program_text = program_text[1:len(program_text)].strip()
            # Debug info
            #print('\n' + self.program_text)
            return character

        token = ''
        string_balance = 0

        while (character != ' ' or string_balance % 2 != 0) and not (Grammar.is_symbol(character) and string_balance % 2 == 0):
            if character == '\"':
                string_balance += 1

            token += character
            current_character += 1
            character = program_text[current_character]

        #print('token - ' + token)

        if Grammar.is_symbol(character):
            current_character -= 1

        if Grammar.is_keyword(token):
            # Debug info
            #print('keyword ' + token)
            self.tok_type = TokenType.KEYWORD

        elif Grammar.is_identifier(token):
            # Debug info
            #print('identifier ' + token)
            self.tok_type = TokenType.IDENTIFIER

        elif Grammar.is_integer(token):
            # Debug info
            #print('integer ' + token)
            self.tok_type = TokenType.INT_CONST

        elif Grammar.is_string(token):
            # Debug info
            #print('string ' + token)

            # remove double quotes from string
            token = token[1:len(token) - 1]
            self.tok_type = TokenType.STRING_CONST
            string_balance = 0

        self.program_text = program_text[current_character + 1:len(program_text)]
        return token

    def __is_blank(self, string):
        if string and string.strip():
            # string is not None AND is not empty or blank
            return False
        # string is None OR is empty or blank
        return True

    def __is_comment(self, line):
        if line.strip().startswith("/**") or line.strip().startswith("*"):
            return True
        return False

    def __is_white_space(self, line):
        return self.__is_blank(line) or self.__is_comment(line)

    def __strip_inline_comment(self, line):
        items = line.split("//")

        if len(items) > 1:
            return items[0]

        return line

    def __strip_line(self, line):
        return line.strip()

    def __read_program_file(self, filename):
        program = open(filename, 'r')
        file_lines = program.readlines()

        return list(file_lines)

    def __filter_program_lines(self, lines):
        text = ''
        for line in lines:
            if not self.__is_white_space(line):
                line = self.__strip_inline_comment(line)
                text += self.__strip_line(line)

        # Debug info
        #print(text)
        return text
