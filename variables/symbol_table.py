from variables.variable_kind import VariableKind

#
# SymbolTable: Keeps all class level and subroutine level variables.
#
class SymbolTable:
    #
    # Creates a new symbol table.
    #
    def __init__(self):
        self.class_level_table = {}
        self.subroutine_level_table = {}

    #
    # Starts a new subroutine scope (i.e., resets the subroutine's
    # symbol table).
    #
    def start_subroutine(self):
        self.subroutine_level_table = {}

    #
    # Defines a new identifier of the given name, type, and kind, and assigns
    # it a running index.
    # STATIC and FIELD identifiers have a class scope, while ARG and VAR
    # identifiers have a subroutine scope.
    #
    def define(self, name, type, kind):
        if not self.__get_by_name(name):
            variable = (name, type, kind, self.var_count(kind))

            if kind is VariableKind.STATIC or kind is VariableKind.FIELD:
                self.class_level_table[name] = variable
            elif kind is VariableKind.ARG or kind is VariableKind.VAR:
                self.subroutine_level_table[name] = variable

    #
    # Returns the number of variables of the given kind already defined in the
    # current scope.
    #
    def var_count(self, kind):
        var_count = 1

        if kind is VariableKind.STATIC or kind is VariableKind.FIELD:
            for class_var_name in self.class_level_table:
                class_var = self.class_level_table[class_var_name]
                if class_var[2] is kind:
                    var_count += 1

        elif kind is VariableKind.ARG or kind is VariableKind.VAR:
            for subroutine_var_name in self.subroutine_level_table:
                subroutine_var = self.subroutine_level_table[subroutine_var_name]
                if subroutine_var[2] is kind:
                    var_count += 1

        return var_count

    #
    # Returns the kind of the named identifier in the current scope.
    # If the identifier is unknown in the current scope, returns NONE.
    #
    def kind_of(self, name):
        variable = self.__get_by_name(name)

        if variable:
            return variable[2]

        return None

    #
    # Returns the type of the named identifier in the current scope.
    #
    def type_of(self, name):
        variable = self.__get_by_name(name)

        if variable:
            return variable[1]

        return None

    #
    # Returns the index assigned to the named identifier.
    #
    def index_of(self, name):
        variable = self.__get_by_name(name)

        if variable:
            return variable[3]

        return 0

    def __get_by_name(self, name):
        if name in self.class_level_table:
            return self.class_level_table[name]
        elif name in self.subroutine_level_table:
            return self.subroutine_level_table[name]

        return None

    def print(self):
        print('-----------------------------\n')
        print('--- class level variables ---\n')
        print('-----------------------------\n')

        for class_var_name in self.class_level_table:
            class_var = self.class_level_table[class_var_name]
            print(('name: {}, type: {}, kind: {}, index: {}\n').format(class_var[0] or '-', class_var[1] or '-', class_var[2].value or '-', class_var[3] or '-'))

        print('\n----------------------------------\n')
        print('--- subroutine level variables ---\n')
        print('----------------------------------\n')

        for subroutine_var_name in self.subroutine_level_table:
            subroutine_var = self.subroutine_level_table[subroutine_var_name]
            print(('name: {}, type: {}, kind: {}, index: {}\n').format(subroutine_var[0] or '-', subroutine_var[1] or '-', subroutine_var[2].value or '-', subroutine_var[3] or '-'))
