from enum import Enum

class VariableKind(Enum):

    STATIC = 'static'
    FIELD = 'field'
    ARG = 'argument'
    VAR = 'local'

    CLASS = 'class'
    SUBROUTINE = 'subroutine'
