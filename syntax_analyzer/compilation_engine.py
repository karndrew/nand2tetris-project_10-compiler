from syntax_analyzer.grammar import Grammar

from tokens.token_type import TokenType
from tokens.tokenizer import Tokenizer
from tokens.keyword import Keyword

from variables.symbol_table import SymbolTable
from variables.variable_kind import VariableKind

from code_generation.vm_writter import VMWritter
from code_generation.segment import Segment
from code_generation.command import Command

from exception.syntax_exception import SyntaxErrorException

import constant

#
# CompilationEngine: generates the compiler's output.
#
class CompilationEngine:
    #
    # Creates a new compilation engine with the given input and output.
    # The next routine called must be compileClass.
    #
    def __init__(self, tokenizer, output_file):
        self.tokenizer = tokenizer
        self.symbol_table = SymbolTable()
        self.vm_writter = VMWritter(output_file)

        self.class_name = ''
        self.step = 1

        self.parameter_count = 0
        self.local_var_count = 0

        self.subroutine_name = None
        self.subroutine_type = None

    #
    # Compiles a complete class.
    #
    def compile_class(self):
        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.CLASS:
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'class')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.IDENTIFIER:
            self.class_name = self.tokenizer.identifier()
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'identifier')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '{':
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, '{')

        self.tokenizer.advance()

        self.compile_class_var_dec()
        self.compile_subroutine_dec()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '}':
            self.step += 1
            self.vm_writter.close()
        else:
            raise SyntaxErrorException(self.step, '}')

    #
    # Compiles a static variable declaration, or a field declaration.
    #
    def compile_class_var_dec(self):
        while self.tokenizer.token_type() is TokenType.KEYWORD and self.__is_class_var(self.tokenizer.keyword()):
            kind = VariableKind(self.tokenizer.keyword().value)
            type = None

            self.step += 1

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.KEYWORD and self.__is_default_type(self.tokenizer.keyword().value):
                type = self.tokenizer.keyword().value
                self.step += 1
            elif self.tokenizer.token_type() is TokenType.IDENTIFIER:
                type = self.tokenizer.identifier()
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, 'type is wrong')

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.IDENTIFIER:
                self.__define_variable(self.tokenizer.identifier(), type, kind)
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, 'varName')

            self.tokenizer.advance()

            while self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ',':
                self.step += 1

                self.tokenizer.advance()

                if self.tokenizer.token_type() is TokenType.IDENTIFIER:
                    self.__define_variable(self.tokenizer.identifier(), type, kind)
                    self.step += 1
                else:
                    raise SyntaxErrorException(self.step, 'varName')

                self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ';':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, ';')

            self.tokenizer.advance()


    #
    # Compiles a complete method, function, or constructor.
    #
    def compile_subroutine_dec(self):
        while self.tokenizer.token_type() is TokenType.KEYWORD and self.__is_subroutine(self.tokenizer.keyword()):

            type = None
            sub_name = ''
            self.subroutine_type = self.tokenizer.keyword()

            self.symbol_table.start_subroutine()

            # add `this` as an argument in every method.
            if self.tokenizer.keyword() is Keyword.METHOD:
                self.__define_variable('this', self.class_name, VariableKind.ARG)

            self.step += 1

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.KEYWORD and\
             self.__is_default_type(self.tokenizer.keyword().value) or\
             self.tokenizer.keyword() is Keyword.VOID:

                type = self.tokenizer.keyword().value
                self.step += 1
            elif self.tokenizer.token_type() is TokenType.IDENTIFIER:
                type = self.tokenizer.identifier()
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, 'type is wrong')

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.IDENTIFIER:
                self.step += 1
                self.subroutine_name = self.tokenizer.identifier()
            else:
                raise SyntaxErrorException(self.step, 'subroutineName')

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, '(')

            self.compile_parameter_list()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, ')')

            self.compile_subroutine_body()

    #
    # Compiles a (possibly empty) parameter list. Does not handle the enclosing "()"
    #
    def compile_parameter_list(self):
        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
            return

        kind = VariableKind.ARG
        type = None

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.__is_default_type(self.tokenizer.keyword().value):
            type = self.tokenizer.keyword().value
            self.step += 1
        elif self.tokenizer.token_type() is TokenType.IDENTIFIER:
            type = self.tokenizer.identifier()
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'type is wrong')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.IDENTIFIER:
            self.__define_variable(self.tokenizer.identifier(), type, kind)
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'varName')

        self.tokenizer.advance()

        while self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ',':
            self.step += 1

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.KEYWORD and\
             self.__is_default_type(self.tokenizer.keyword().value):

                type = self.tokenizer.keyword().value
                self.step += 1
            elif self.tokenizer.token_type() is TokenType.IDENTIFIER:
                type = self.tokenizer.identifier()
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, 'type is wrong')

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.IDENTIFIER:
                self.__define_variable(self.tokenizer.identifier(), type, kind)
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, 'varName')

            self.tokenizer.advance()

    #
    # Compiles a subroutine's body.
    #
    def compile_subroutine_body(self):

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '{':
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, '{')

        self.tokenizer.advance()

        self.local_var_count = 0

        while self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.VAR:
            self.compile_var_dec()

        self.vm_writter.write_function(self.class_name + '.' + self.subroutine_name,  self.local_var_count)

        if self.subroutine_type is Keyword.CONSTRUCTOR:
            fields_count = self.symbol_table.var_count(VariableKind.FIELD)
            self.vm_writter.write_push(Segment.CONST, fields_count - 1)
            self.vm_writter.write_call(constant.MEMORY_ALLOC_METHOD, 1)
            self.vm_writter.write_pop(Segment.POINTER, 0)

        elif self.subroutine_type is Keyword.METHOD:
            self.vm_writter.write_push(Segment.ARG, 0)
            self.vm_writter.write_pop(Segment.POINTER, 0)

        self.compile_statements()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '}':
            self.step += 1

            self.tokenizer.advance()
        else:
            raise SyntaxErrorException(self.step, '}')

    #
    # Compiles a var declaration.
    #
    def compile_var_dec(self):

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.VAR:
            self.step += 1

            kind = VariableKind.VAR
            type = None

            self.tokenizer.advance()
        else:
            return

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.__is_default_type(self.tokenizer.keyword().value):
            type = self.tokenizer.keyword().value
            self.step += 1
        elif self.tokenizer.token_type() is TokenType.IDENTIFIER:
            type = self.tokenizer.identifier()
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'type is wrong')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.IDENTIFIER:
            self.__define_variable(self.tokenizer.identifier(), type, kind)
            self.step += 1
            self.local_var_count += 1
        else:
            raise SyntaxErrorException(self.step, 'varName')

        self.tokenizer.advance()

        while self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ',':
            self.step += 1

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.IDENTIFIER:
                self.__define_variable(self.tokenizer.identifier(), type, kind)
                self.step += 1
                self.local_var_count += 1
            else:
                raise SyntaxErrorException(self.step, 'varName')

            self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ';':
            self.step += 1

            self.tokenizer.advance()
        else:
            raise SyntaxErrorException(self.step, ';')

    #
    # Compiles a sequence of statements. Does not handle the enclosing "()"
    #
    def compile_statements(self):
        statement_keywords = ('let', 'if', 'while', 'do', 'return')

        while self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword().value in statement_keywords:

            if self.tokenizer.keyword().value not in statement_keywords:
                return

            if self.tokenizer.keyword().value in statement_keywords:
                if self.tokenizer.keyword() is Keyword.LET:
                    self.compile_let()
                    self.tokenizer.advance()
                elif self.tokenizer.keyword() is Keyword.IF:
                    self.compile_if()
                elif self.tokenizer.keyword() is Keyword.WHILE:
                    self.compile_while()
                    self.tokenizer.advance()
                elif self.tokenizer.keyword() is Keyword.DO:
                    self.compile_do()
                    self.tokenizer.advance()
                elif self.tokenizer.keyword() is Keyword.RETURN:
                    self.compile_return()
                    self.tokenizer.advance()

    #
    # Compiles a let statement.
    #
    def compile_let(self):

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.LET:
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'let')

        self.tokenizer.advance()

        index_assignment = False

        if self.tokenizer.token_type() is TokenType.IDENTIFIER:
            variable = self.__get_variable(self.tokenizer.identifier())

            kind = variable[2]
            index = variable[3]

            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'varName')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '[':
            self.step += 1

            self.tokenizer.advance()
            self.compile_expression()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ']':
                index_assignment = True

                self.vm_writter.write_push(Segment(kind.value), index)
                self.vm_writter.write_arithmetic(Command.ADD)

                self.step += 1
                self.tokenizer.advance()
            else:
                raise SyntaxErrorException(self.step, ']')

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '=':
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, '=')

        self.tokenizer.advance()
        self.compile_expression()

        if kind:
            if variable[1] == constant.ARRAY_TYPE and index_assignment:
                self.vm_writter.write_pop(Segment.TEMP, 0)
                self.vm_writter.write_pop(Segment.POINTER, 1)
                self.vm_writter.write_push(Segment.TEMP, 0)
                self.vm_writter.write_pop(Segment.THAT, 0)
            else:
                if kind is VariableKind.FIELD:
                    self.vm_writter.write_pop(Segment.THIS, index)
                else:
                    self.vm_writter.write_pop(Segment(kind.value), index)

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ';':
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, ';')

    #
    # Compiles an if statement, possibly with a trailing else clause.
    #
    def compile_if(self):

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.IF:
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'if')

        self.tokenizer.advance()

        line_number = str(self.step)

        if_label_true = "IF_TRUE_" + line_number
        if_label_otherwise = "IF_FALSE_" + line_number
        if_label_end = "IF_END_" + line_number

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
            self.step += 1

            self.tokenizer.advance()
            self.compile_expression()

            self.vm_writter.write_if(if_label_true)
            self.vm_writter.write_goto(if_label_otherwise)

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, ')')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '{':
            self.step += 1

            self.tokenizer.advance()

            self.vm_writter.write_label(if_label_true)

            self.compile_statements()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '}':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, '}')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.ELSE:
            self.step += 1

            self.tokenizer.advance()

            self.vm_writter.write_goto(if_label_end)

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '{':
                self.step += 1

                self.tokenizer.advance()

                self.vm_writter.write_label(if_label_otherwise)

                self.compile_statements()

                self.vm_writter.write_label(if_label_end)

                if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '}':
                    self.step += 1

                    self.tokenizer.advance()
                else:
                    raise SyntaxErrorException(self.step, '}')
        else:
            self.vm_writter.write_label(if_label_otherwise)
            return

    #
    # Compiles a while statement.
    #
    def compile_while(self):

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.WHILE:
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'while')

        self.tokenizer.advance()

        line_number = str(self.step)

        loop_start_label = "LOOP_START_" + line_number
        loop_end_label = "LOOP_END_" + line_number

        self.vm_writter.write_label(loop_start_label)

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
            self.step += 1

            self.tokenizer.advance()
            self.compile_expression()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, ')')

        self.vm_writter.write_arithmetic(Command.NOT)
        self.vm_writter.write_if(loop_end_label)

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '{':
            self.step += 1

            self.tokenizer.advance()
            self.compile_statements()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '}':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, '}')

            self.vm_writter.write_goto(loop_start_label)
            self.vm_writter.write_label(loop_end_label)

    #
    # Compiles a do statement.
    #
    def compile_do(self):
        reference_method = False

        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.DO:
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'do')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.IDENTIFIER:
            name = self.tokenizer.identifier()
            variable = self.__get_variable(name)
            subroutine_name = ''

            if variable:
                reference_method = True
                segment = self.__get_segment(variable[2])

                self.vm_writter.write_push(segment, variable[3])

                subroutine_name = variable[1]
            else:
                subroutine_name = name

            self.step += 1

            self.tokenizer.advance()

            if self.tokenizer.token_type() is TokenType.SYMBOL and\
             self.tokenizer.symbol() == '(' or self.tokenizer.symbol() == '.':

                if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
                    subroutine_name = self.class_name + '.' + subroutine_name
                    self.vm_writter.write_push(Segment.POINTER, 0)

                    self.compile_expression_list()

                    reference_method = True

                    self.step += 1

                    if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                        self.step += 1
                    else:
                        raise SyntaxErrorException(self.step, ')')

                elif self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '.':
                    self.step += 1

                    self.tokenizer.advance()

                    if self.tokenizer.token_type() is TokenType.IDENTIFIER:
                        subroutine_name += '.' + self.tokenizer.identifier()
                        self.step += 1
                    else:
                        raise SyntaxErrorException(self.step, 'subroutineName')

                    self.tokenizer.advance()

                    if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
                        self.step += 1

                        self.compile_expression_list()

                        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                            self.step += 1
                        else:
                            raise SyntaxErrorException(self.step, ')')

                # count 'this' as extra parameter of a method.
                if reference_method:
                    self.parameter_count += 1

                self.vm_writter.write_call(subroutine_name, self.parameter_count)
                self.vm_writter.write_pop(Segment(Segment.TEMP), 0)
            else:
                raise SyntaxErrorException(self.step, '( or .')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ';':
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, ';')

    #
    # Compiles a return statement.
    #
    def compile_return(self):
        if self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword() is Keyword.RETURN:
            self.step += 1
        else:
            raise SyntaxErrorException(self.step, 'return')

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ';':
            self.vm_writter.write_push(Segment.CONST, 0)
            self.step += 1
        else:
            self.compile_expression()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ';':
                self.step += 1
            else:
                raise SyntaxErrorException(self.step, ';')

        self.vm_writter.write_return()

    #
    # Compiles an expression.
    #
    def compile_expression(self):
        operators = ('+', '-', '*', '/', '&', '|', '<', '>', '=')

        self.compile_term()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() in operators:
            operator = self.tokenizer.symbol()

            self.step += 1

            self.tokenizer.advance()

            self.compile_term()

            if operator == '*':
                self.vm_writter.write_call(constant.MATH_MULTIPLY_METHOD, 2)
            elif operator == '/':
                self.vm_writter.write_call(constant.MATH_DIVIDE_METHOD, 2)
            else:
                self.vm_writter.write_arithmetic(Command(operator))

    #
    # Compiles a term. If the current token is an identifier, the routine must distinguish
    # between a variable, an array entry, or a subroutine call. A single look-ahead
    # token, which may be one of "[", "(", or ".", suffices to distinguish between the
    # possibilities. Any other token is not part of this term and should not be advanced
    # over.
    #
    def compile_term(self):
        unary_operators = ('-', '~')
        keyword_constants = ('true', 'false', 'null', 'this')

        if self.tokenizer.token_type() is TokenType.STRING_CONST:
            string_value = self.tokenizer.string_val()
            size = len(string_value)
            chars = [letter for letter in string_value]

            self.vm_writter.write_push(Segment.CONST, size)
            self.vm_writter.write_call(constant.STRING_NEW_METHOD, 1)

            for char in chars:
                self.vm_writter.write_push(Segment.CONST, ord(char))
                self.vm_writter.write_call(constant.STRING_APPEND_CHAR_METHOD, 2)

            self.step += 1

            self.tokenizer.advance()

        # if exp is a number n:
        elif self.tokenizer.token_type() is TokenType.INT_CONST:
            # output 'push n'
            self.vm_writter.write_push(Segment.CONST, self.tokenizer.int_val())
            self.step += 1

            self.tokenizer.advance()

        elif self.tokenizer.token_type() is TokenType.KEYWORD and self.tokenizer.keyword().value in keyword_constants:
            if self.tokenizer.keyword().value == 'null' or self.tokenizer.keyword().value == 'false':
                self.vm_writter.write_push(Segment.CONST, 0)
            elif self.tokenizer.keyword().value == 'true':
                self.vm_writter.write_push(Segment.CONST, 1)
                self.vm_writter.write_arithmetic(Command.NEG)
            elif self.tokenizer.keyword().value == 'this':
                self.vm_writter.write_push(Segment.POINTER, 0)

            self.step += 1

            self.tokenizer.advance()

        elif self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
            self.step += 1

            self.tokenizer.advance()
            self.compile_expression()

            if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                self.step += 1

                self.tokenizer.advance()
            else:
                raise SyntaxErrorException(self.step, ')')

        elif self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() in unary_operators:
            self.step += 1

            operator = self.tokenizer.symbol()

            self.tokenizer.advance()

            self.compile_term()

            if operator == '-':
                self.vm_writter.write_arithmetic(Command.NEG)
            elif operator == '~':
                self.vm_writter.write_arithmetic(Command.NOT)

        elif self.tokenizer.token_type() is TokenType.IDENTIFIER:
            name = self.tokenizer.identifier()
            variable = self.__get_variable(name)
            subroutine_name = ''
            reference_method = False

            if variable:
                segment = self.__get_segment(variable[2])

                if segment is Segment.THIS:
                    subroutine_name = variable[1] + '.'
                    reference_method = True

                self.vm_writter.write_push(segment, variable[3])
            else:
                subroutine_name = name + '.'

            self.step += 1

            self.tokenizer.advance()

            #
            # If the case of `subroutineCall`
            #
            if self.tokenizer.token_type() is TokenType.SYMBOL and\
             self.tokenizer.symbol() == '(' or self.tokenizer.symbol() == '.':

                if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
                    self.step += 1

                    self.compile_expression_list()

                    if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                        self.step += 1
                    else:
                        raise SyntaxErrorException(self.step, ')')

                elif self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '.':
                    self.step += 1

                    self.tokenizer.advance()

                    if self.tokenizer.token_type() is TokenType.IDENTIFIER:
                        subroutine_name = subroutine_name + self.tokenizer.identifier()
                        self.step += 1
                    else:
                        raise SyntaxErrorException(self.step, 'subroutineName')

                    self.tokenizer.advance()

                    if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '(':
                        self.step += 1

                        self.compile_expression_list()

                        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
                            self.step += 1

                            self.tokenizer.advance()
                        else:
                            raise SyntaxErrorException(self.step, ')')

                    if reference_method:
                        self.parameter_count += 1

                    self.vm_writter.write_call(subroutine_name, self.parameter_count)
            #
            # If the case of `varName[]`
            #
            elif self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == '[':
                self.step += 1

                self.tokenizer.advance()
                self.compile_expression()

                self.vm_writter.write_arithmetic(Command.ADD)
                self.vm_writter.write_pop(Segment.POINTER, 1)
                self.vm_writter.write_push(Segment.THAT, 0)

                if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ']':
                    self.step += 1

                    self.tokenizer.advance()
                else:
                    raise SyntaxErrorException(self.step, ']')

    #
    # Compiles a (possibly empty) comma-separated list of expressions.
    #
    def compile_expression_list(self):
        self.parameter_count = 0

        self.tokenizer.advance()

        if self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ')':
            return

        self.compile_expression()
        self.parameter_count += 1

        if self.tokenizer.token_type() is not TokenType.SYMBOL:
            self.tokenizer.advance()

        while self.tokenizer.token_type() is TokenType.SYMBOL and self.tokenizer.symbol() == ',':
            self.step += 1

            self.tokenizer.advance()
            self.compile_expression()

            self.parameter_count += 1

    def __is_default_type(self, token):
        return token in ('int', 'char', 'boolean')

    def __is_class_var(self, keyword):
        return keyword is Keyword.STATIC or keyword is Keyword.FIELD

    def __is_subroutine(self, keyword):
        return self.tokenizer.keyword() is Keyword.CONSTRUCTOR or\
        self.tokenizer.keyword() is Keyword.FUNCTION or\
        self.tokenizer.keyword() is Keyword.METHOD

    def __is_variable_defined(self, name):
        return self.symbol_table.index_of(name)

    def __define_variable(self, name, type, kind):
        index = self.__is_variable_defined(name)

        if not index:
            index = self.symbol_table.var_count(kind)
            self.symbol_table.define(name, type, kind)

    def __get_variable(self, name):
        index = self.__is_variable_defined(name)

        if index > 0:
            kind = self.symbol_table.kind_of(name)
            index = self.symbol_table.index_of(name) - 1
            type = self.symbol_table.type_of(name)

            return (name, type, kind, index)

        return None

    def __get_segment(self, kind):
        if kind == VariableKind.FIELD:
            return Segment.THIS
        else:
            return Segment(kind.value)
