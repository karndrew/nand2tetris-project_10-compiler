import re

#
# Language gramar constructs.
#
class Grammar:

    symbol_mappings = {
        '<':'&lt;',
        '>':'&gt;',
        '\"':'&quot;',
        '&':'&amp;',
    }

    @staticmethod
    def is_keyword(token):
        keywords = ['class', 'constructor', 'function', 'method', 'field', 'static',
        'var', 'int', 'char', 'boolean', 'void', 'true', 'false', 'null', 'this',
        'let', 'do', 'if', 'else', 'while', 'return']

        return token in keywords

    @staticmethod
    def is_symbol(token):
        symbols = ['{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*',
        '/', '&', '|', '<', '>', '=', '~']

        return token in symbols

    @staticmethod
    def is_integer(token):
        try:
            integer = int(token)
            return integer >= 0 and integer <= 32767
        except ValueError:
            return False

    @staticmethod
    def is_string(token):
        return token.startswith("\"") and token.endswith("\"")

    @staticmethod
    def is_identifier(token):
        return re.search(r"^[^0-9][a-zA-Z0-9_]*$", token)

    @classmethod
    def map_symbol(cls, symbol):
        if symbol in cls.symbol_mappings:
            return cls.symbol_mappings[symbol]

        return symbol
