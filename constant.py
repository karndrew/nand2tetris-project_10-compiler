ARRAY_TYPE = 'Array'

MEMORY_ALLOC_METHOD = 'Memory.alloc'
MATH_MULTIPLY_METHOD = 'Math.multiply'
MATH_DIVIDE_METHOD = 'Math.divide'
STRING_NEW_METHOD = 'String.new'
STRING_APPEND_CHAR_METHOD = 'String.appendChar'
