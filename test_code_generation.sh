#!/bin/bash

echo 'Seven Test'
python3 compiler/compiler.py Seven

./../../tools/TextComparer.sh Seven/expected/Main.vm Seven/Main.vm

echo ''
echo 'ConvertToBin Test'
python3 compiler/compiler.py ConvertToBin

./../../tools/TextComparer.sh ConvertToBin/expected/Main.vm ConvertToBin/Main.vm

echo ''
echo 'Square Test'
python3 compiler/compiler.py Square

./../../tools/TextComparer.sh Square/expected/Main.vm Square/Main.vm
./../../tools/TextComparer.sh Square/expected/SquareGame.vm Square/SquareGame.vm
./../../tools/TextComparer.sh Square/expected/Square.vm Square/Square.vm

echo ''
echo 'Average Test'
python3 compiler/compiler.py Average

./../../tools/TextComparer.sh Average/expected/Main.vm Average/Main.vm

echo ''
echo 'Pong Test'
python3 compiler/compiler.py Pong

./../../tools/TextComparer.sh Pong/expected/PongGame.vm Pong/PongGame.vm
./../../tools/TextComparer.sh Pong/expected/Main.vm Pong/Main.vm
./../../tools/TextComparer.sh Pong/expected/Ball.vm Pong/Ball.vm
./../../tools/TextComparer.sh Pong/expected/Bat.vm Pong/Bat.vm

echo ''
echo 'ComplexArrays Test'
python3 compiler/compiler.py ComplexArrays

./../../tools/TextComparer.sh ComplexArrays/expected/Main.vm ComplexArrays/Main.vm
