import sys
import os

from os import listdir

from tokens.token_type import TokenType
from tokens.tokenizer import Tokenizer

from syntax_analyzer.compilation_engine import CompilationEngine
from syntax_analyzer.grammar import Grammar

class Compiler:
    def __init__(self, filename):
        self.input_file = filename
        self.dir = ''

    def analyze(self):
        self.__handle_input(self.input_file)

    def __handle_input(self, file):
        if os.path.isfile(file) and file.endswith('.jack'):
            self.__handle_file(file)
        elif os.path.isdir(file):

            if self.dir:
                dir = self.dir + '/' + file
            else:
                dir = file

            file_list = os.listdir(file)

            for file_dir in file_list:
                self.__handle_input(dir + '/' + file_dir)

    def __handle_file(self, file):
        tokenizer = Tokenizer(file)

        output_file_name = self.__adjust_file_name(file)
        self.__remove_file(output_file_name)

        compilation_engine = CompilationEngine(tokenizer, output_file_name)
        compilation_engine.compile_class()

    def __remove_file(cls, filename):
        if os.path.exists(filename):
            os.remove(filename)

    def __adjust_file_name(self, filename):
        if filename.endswith('.jack'):
            filename = filename.replace(".jack", ".vm")
        else:
            file = filename[filename.rindex('/') + 1:len(filename)] + '.vm'
            dir = filename + '/'
            filename = dir + file

        return filename

def main():
    assembler = Compiler(sys.argv[1])
    assembler.analyze()



if __name__ == "__main__":
    main()
