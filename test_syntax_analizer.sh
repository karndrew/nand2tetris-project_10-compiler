#!/bin/bash

echo 'ExpressionLessSquare Test'
python3 compiler/syntax_analyzer.py ExpressionLessSquare

./../../tools/TextComparer.sh ExpressionLessSquare/SquareGame.xml ExpressionLessSquare/SquareGameC.xml
./../../tools/TextComparer.sh ExpressionLessSquare/Square.xml ExpressionLessSquare/SquareC.xml
./../../tools/TextComparer.sh ExpressionLessSquare/Main.xml ExpressionLessSquare/MainC.xml

echo ''
echo 'Square Test'
python3 compiler/syntax_analyzer.py Square

./../../tools/TextComparer.sh Square/SquareGame.xml Square/SquareGameC.xml
./../../tools/TextComparer.sh Square/Square.xml Square/SquareC.xml
./../../tools/TextComparer.sh Square/Main.xml Square/MainC.xml

echo ''
echo 'ArrayTest Test'
python3 compiler/syntax_analyzer.py ArrayTest

./../../tools/TextComparer.sh ArrayTest/Main.xml ArrayTest/MainC.xml
