from enum import Enum

class Command(Enum):

    ADD = '+'
    SUB = '-'
    NEG = ''
    EQ = '='
    GT = '>'
    LT = '<'
    AND = '&'
    OR = '|'
    NOT = '~'
