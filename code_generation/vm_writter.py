from variables.variable_kind import VariableKind

#
# SymbolTable: Keeps all class level and subroutine level variables.
#
class VMWritter:
    #
    # Creates a new symbol table.
    #
    def __init__(self, output_file):
        self.__open_program_file(output_file)

    #
    # Writes a VM push command.
    #
    def write_push(self, segment, index):
        self.output_file.write(("push {} {}\n").format(segment.value, index))

    #
    # Writes a VM pop command.
    #
    def write_pop(self, segment, index):
        self.output_file.write(("pop {} {}\n").format(segment.value, index))

    #
    # Writes a VM arithmetic-logical command.
    #
    def write_arithmetic(self, command):
        self.output_file.write(("{}\n").format(command.name.lower()))

    #
    # Writes a VM label command.
    #
    def write_label(self, label):
        self.output_file.write(("label {}\n").format(label))

    #
    # Writes a VM goto command.
    #
    def write_goto(self, label):
        self.output_file.write(("goto {}\n").format(label))

    #
    # Writes a VM if-goto command.
    #
    def write_if(self, label):
        self.output_file.write(("if-goto {}\n").format(label))

    #
    # Writes a VM call command.
    #
    def write_call(self, name, n_args):
        self.output_file.write(("call {} {}\n").format(name, n_args))

    #
    # Writes a VM function command.
    #
    def write_function(self, name, n_locals):
        self.output_file.write(("function {} {}\n").format(name, n_locals))

    #
    # Writes a VM return command.
    #
    def write_return(self):
        self.output_file.write("return\n")

    #
    # Closes the output file.
    #
    def close(self):
        self.output_file.close()

    def __open_program_file(self, filename):
        self.output_file = open(filename, 'a')
