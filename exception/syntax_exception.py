class SyntaxErrorException(Exception):
    """Exception raised for errors in the code compilation.

    Attributes:
        line -- input line which had the error
        expected -- symbol which should have been secified
        message -- explanation of the error
    """

    def __init__(self, line, expected, message="Syntax error on line {}. Expected statement was \'{}\'."):
        self.line = line
        self.expected = expected
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return self.message.format(self.line, self.expected)
